<?php
/**
 * @file
 * Drush commands for dnode_socialgraph.
 */

/**
 * Implements of hook_drush_command().
 */
function dnode_socialgraph_drush_command() {
  $items['dnode-socialgraph-status'] = array(
    'callback' => 'dnode_socialgraph_status',
  );
  $items['dnode-socialgraph-sync'] = array(
    'callback' => 'dnode_socialgraph_sync',
  );
  return $items;
}

/**
 * Display status between user $a and $b in user_relations ship type id $sg_id.
 */
function dnode_socialgraph_status($sg_id, $a, $b = NULL) {
  $relationship_types = user_relationships_types_load();
  $types = array(
    'getFollowers',
    'countFollowers',
    'getFollowing',
    'countFollowing',
  );
  foreach ($types as $opt) {
    dnode_rpc('dnode_socialgraph', $opt, array('ur_' . $sg_id, $a), function($err, $data) use ($sg_id, $a, $opt) {
      drush_print("$opt(ur_$sg_id, $a)" . print_r($data, TRUE));
    });
  }
  if ($b) {
    $types = array(
      'isFollowing',
      'hasFollower',
      'getCommonFollowing',
      'getCommonFollowers',
      'getDifferentFollowing',
      'getDifferentFollowers',
    );
    foreach ($types as $opt) {
      dnode_rpc('dnode_socialgraph', $opt, array('ur_' . $sg_id, $a, $b), function($err, $data) use ($sg_id, $a, $b, $opt) {
         drush_print("$opt(ur_$sg_id, $a, $b)" . print_r($data, TRUE));
      });
    }

  }
}

/**
 * Sync an existing socialgraph from user_relationship to dnode_socialgraph.
 */
function dnode_socialgraph_sync($sg_id) {
  drush_print(dt('Not implemented yet.'));
}
