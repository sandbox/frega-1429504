var socialGraph = require('./lib/socialgraph').socialGraph;
// an object storing
var sgMap = {};

// load config - @todo: proper configuration injection
if (DrupalDnode) {
  var config = DrupalDnode.getModuleConfig('dnode_socialgraph');
} else {
  var config = require('./config.json');
}


function socialGraphFactory(socialgraph_id) {
  if (typeof sgMap[socialgraph_id]=='undefined') {
    sgMap[socialgraph_id] = new socialGraph({prefix: 'sg-'+socialgraph_id});
  }
  return sgMap[socialgraph_id];
}

var methods = {
  follow: function(socialgraph_id, a, b, cb) {
    socialGraphFactory(socialgraph_id).follow(a, b, cb);
  },
  unfollow: function(socialgraph_id, a, b, cb) {
    socialGraphFactory(socialgraph_id).unfollow(a, b, cb);
  },
  friend: function(socialgraph_id, a, b, cb) {
    var sg = socialGraphFactory(socialgraph_id);
    sg.follow(a, b, function(err, data) {
      if (err) {
        return cb(err);
      }
      sg.follow(b,a, cb);
    });
  },
  unfriend: function(socialgraph_id, a, b, cb) {
    var sg = socialGraphFactory(socialgraph_id);
    sg.unfollow(a, b, function(err, data) {
      if (err) {
        return cb(err);
      }
      sg.unfollow(b,a, cb);
    });
  },
  getFollowers: function(socialgraph_id, a, cb) {
    socialGraphFactory(socialgraph_id).getFollowers(a, cb);
  },
  countFollowers: function(socialgraph_id, a, cb) {
    socialGraphFactory(socialgraph_id).countFollowers(a, cb);
  },
  getFollowing: function(socialgraph_id, a, cb) {
    socialGraphFactory(socialgraph_id).getFollowing(a, cb);
  },
  countFollowing: function(socialgraph_id, a, cb) {
    socialGraphFactory(socialgraph_id).countFollowing(a, cb);
  },
  isFollowing: function(socialgraph_id, a, b, cb) {
    socialGraphFactory(socialgraph_id).isFollowing(a, b, cb);
  },
  hasFollower: function(socialgraph_id, a, b, cb) {
    socialGraphFactory(socialgraph_id).hasFollower(a, b, cb);
  },
  getCommonFollowers: function(socialgraph_id, a, b, cb) {
    socialGraphFactory(socialgraph_id).getCommonFollowers(a, b, cb);
  },
  getCommonFollowing: function(socialgraph_id, a, b, cb) {
    socialGraphFactory(socialgraph_id).getCommonFollowing(a, b, cb);
  },
  getDifferentFollowing: function(socialgraph_id, a, b, cb) {
    socialGraphFactory(socialgraph_id).getDifferentFollowing(a, b, cb);
  },
  getDifferentFollowers: function(socialgraph_id, a, b, cb) {
    socialGraphFactory(socialgraph_id).getDifferentFollowers(a, b, cb);
  },
  // remove a node (e.g. user) completely
  remove: function(socialgraph_id, a, cb) {
    socialGraphFactory(socialgraph_id).remove(a, cb);
  }
 }

for (var i in methods) {
  exports[i] = methods[i];
}

exports.config = {
  serverId: 'dnode_socialgraph',
  port: 5056
};
