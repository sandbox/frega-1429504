var redback = require('redback');
var _ = require('underscore');

var socialGraph = function(config) {
  this.setConfig(config);
}

socialGraph.prototype.setConfig = function(config) {
  config = config || {};
  config.prefix = config.prefix || '';
  config.forceMutual = config.forceMutual || false;
  this.redbackClient = require('redback').createClient(config.port || 6379, config.host || '127.0.0.1', config);
  this.config = config;
};

// create a user/node specific social graph object
socialGraph.prototype._getUserSocialGraph = function(id) {
  return this.redbackClient.createSocialGraph(id, this.config.prefix);
};

socialGraph.prototype.follow = function(a, b, cb) {
  if (this.config.forceMutual) {
    this._getUserSocialGraph(a).follow(b, function(err, data) {
      if (!err) {
        this._getUserSocialGraph(b).follow(a, cb);
      } else {
        cb(err);
      }
    });
  } else {
    this._getUserSocialGraph(a).follow(b, cb);
  }
};
socialGraph.prototype.unfollow = function(a, b, cb) {
  console.log(this.config, this);
  if (this.config.forceMutual) {
    this._getUserSocialGraph(a).unfollow(b, function(err, data) {
      if (!err) {
        this._getUserSocialGraph(b).unfollow(a, cb);
      } else {
        cb(err);
      }
    });
  } else {
    this._getUserSocialGraph(a).unfollow(b, cb);
  }
};
socialGraph.prototype.getFollowers = function(a, cb) {
  this._getUserSocialGraph(a).getFollowers(cb);
};
socialGraph.prototype.countFollowers = function(a, cb) {
  this._getUserSocialGraph(a).countFollowers(cb);
};
socialGraph.prototype.getFollowing = function(a, cb) {
  this._getUserSocialGraph(a).getFollowing(cb);
};
socialGraph.prototype.countFollowing = function(a, cb) {
  this._getUserSocialGraph(a).countFollowing(cb);
};
socialGraph.prototype.isFollowing = function(a, b, cb) {
  this._getUserSocialGraph(a).isFollowing(b, cb);
};
socialGraph.prototype.hasFollower = function(a, b, cb) {
  this._getUserSocialGraph(a).hasFollower(b, cb);
};
socialGraph.prototype.getCommonFollowers = function(a, b, cb) {
  this._getUserSocialGraph(a).getCommonFollowers(b, cb);
};
socialGraph.prototype.getCommonFollowing = function(a, b, cb) {
  this._getUserSocialGraph(a).getCommonFollowing(b, cb);
};
socialGraph.prototype.getDifferentFollowing = function(a, b, cb) {
  this._getUserSocialGraph(a).getDifferentFollowing(b, cb);
};
socialGraph.prototype.getDifferentFollowers = function(a, b, cb) {
  this._getUserSocialGraph(a).getDifferentFollowers(b, cb);
};

// remove an "node in the social graph" (e.g. a user) completely
socialGraph.prototype.remove = function(a, cb) {
  var sg = this._getUserSocialGraph(a);
  sg.getFollowing(a, function(err, data) {
    if (!err) {
      // @todo: non-blocking?
      _.each(data, function(element,index,list) {
        sg.unfollow(a);
      });
    }
    cb();
  });
};

exports.socialGraph = socialGraph;